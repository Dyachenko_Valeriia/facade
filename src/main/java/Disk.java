public class Disk {
    private boolean disk = false; //есть ли диск в проигрывателе

    public boolean isDisk() {
        return disk;
    }
    public void insert(){
        disk = true;
    }
    public void pull(){
        disk = false;
    }
}
