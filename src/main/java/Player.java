public class Player {
    private Power power = new Power();
    private Disk disk = new Disk();
    private Music music = new Music();

    public String listenMusic(){
        power.on();
        disk.insert();
        return music.listenMusic(disk);
    }
}
